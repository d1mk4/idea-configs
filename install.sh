#!/bin/bash

echo "Installing my keymap configs..."

for i in $HOME/Library/Preferences/IntelliJIdea*/keymaps \
         $HOME/Library/Preferences/IdeaIC*/keymaps \
         $HOME/Library/Preferences/AndroidStudio*/keymaps \
         $HOME/.IntelliJIdea*/config/keymaps \
         $HOME/.IdeaIC*/config/keymaps \
         $HOME/.AndroidStudio*/config/keymaps
do
  cp -frv $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/keymaps/* $i 2> /dev/null
done

echo "Installing Square codestyles configs..."

for i in $HOME/Library/Preferences/IntelliJIdea*/codestyles \
         $HOME/Library/Preferences/IdeaIC*/codestyles \
         $HOME/Library/Preferences/AndroidStudio*/codestyles \
         $HOME/.IntelliJIdea*/config/codestyles \
         $HOME/.IdeaIC*/config/codestyles \
         $HOME/.AndroidStudio*/config/codestyles
do
  cp -frv $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/codestyles/* $i 2> /dev/null
done


echo "Done."
echo ""
echo "Restart IntelliJ and/or AndroidStudio, go to preferences, and apply my keymap & Square codestyle."